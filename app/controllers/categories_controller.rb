class CategoriesController < ApplicationController
  before_action :authorize_request
  before_action :set_category, only: [:show, :update, :destroy]
  # GET /api/categories
  def index
    @categories = Category.all
    json_response(@categories)
  end

  # GET /api/category/:id
  def show
    json_response(@category)
  end

  # POST /api/categories
  def create
    @category = Category.create!(category_params)
    json_response(@category, :created)
  end

  def update
    @category.update(category_params)
    head :no_content
  end

  def destroy
    @category.destroy
    head :no_content
  end

  private

  def category_params
    params.permit(:name)
  end

  def set_category
    @category = Category.find(params[:id])
  end
end
