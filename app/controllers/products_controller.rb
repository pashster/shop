class ProductsController < ApplicationController
  before_action :authorize_request
  before_action :set_product, only: [:show, :update, :destroy]

  # GET /api/products
  def index
    @products = Product.all
    json_response(@products)
  end

  # GET  /api/products/:id
  def show
    json_response(@product)
  end

  # POST /api/products
  def create
    @product = current_user.products.new(product_params)
    @product.date_of_receipt = Date.current.to_formatted_s(:db)
    @product.save!
    json_response(@product, :created)
  end

  # PUT  /api/products/:id
  def update
    if @product.update(product_params)
      head :no_content
    else
      response = {message: Message.product_not_updated}
      json_response(response, :bad_request)
    end
  end

  # DELETE /api/products/:id
  def destroy
    @product.destroy
    head :no_content
  end

  private

  def product_params
    params.permit(
        :name,
        :desc,
        :date_of_receipt,
        :quantity,
        :price,
        :category_id,
        :cost_price,
        :user_id
    )
  end

  def set_product
    @product = Product.find(params[:id])
  end
end
