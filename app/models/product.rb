class Product < ApplicationRecord
  belongs_to :category
  belongs_to :user

  validates_presence_of :name, :date_of_receipt, :quantity, :price, :cost_price
  validates :name, length: {minimum: 3}
  validates :quantity, numericality: true
  validates :price, numericality: true
  validates :cost_price, numericality: true
end
