class CreateProducts < ActiveRecord::Migration[5.0]
  def change
    create_table :products do |t|
      t.string :name
      t.text :desc
      t.datetime :date_of_receipt
      t.integer :quantity
      t.integer :price
      t.references :category, foreign_key: true
      t.integer :cost_price

      t.timestamps
    end
  end
end
