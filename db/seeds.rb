# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)

user = User.create!(
    name: "Test",
    email: "qwe@qwe.qwe",
    password: "qweqwe",
    password_confirmation: "qweqwe"
)

4.times do
  Category.create!(
      name: Faker::Pokemon.name
  )
end

15.times do |n|
  Product.create!(
      name: Faker::Cat.name,
      desc: Faker::BackToTheFuture.quote,
      date_of_receipt: Faker::Date.between_except((n + 1).year.ago, (n + 1).year.from_now, Date.today),
      quantity: (n + 64),
      price: Faker::Number.number(4),
      category_id: rand(1..Category.all.count),
      cost_price: (n + 94),
      user_id: user.id
  )
end