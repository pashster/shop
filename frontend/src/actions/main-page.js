import {get} from './request'
import {toastr} from 'react-redux-toastr'

export const FETCH_PRODUCTS = '@@MAIN_PAGE/FETCH_PRODUCTS';
export const FETCH_PRODUCTS_SUCCESS = '@@MAIN_PAGE/FETCH_PRODUCTS_SUCCESS';
export const FETCH_PRODUCTS_FAILURE = '@@MAIN_PAGE/FETCH_PRODUCTS_FAILURE';

export const FETCH_CATEGORY = '@@MAIN_PAGE/FETCH_CATEGORY';
export const FETCH_CATEGORY_SUCCESS = '@@MAIN_PAGE/FETCH_CATEGORY_SUCCESS';
export const FETCH_CATEGORY_FAILURE = '@@MAIN_PAGE/FETCH_CATEGORY_FAILURE';

export function fetchProducts() {
  return (dispatch) => {
    const token = localStorage.getItem('token');
    dispatch({type: FETCH_PRODUCTS});
    return get('/api/products', token).then((response) => {
      return dispatch({type: FETCH_PRODUCTS_SUCCESS, response})
    }).catch((error) => {
      toastr.error(error.message);
      return dispatch({type: FETCH_PRODUCTS_FAILURE});
    })
  }
}

export function fetchCategory() {
  return(dispatch) => {
    const token = localStorage.getItem('token');
    dispatch({type: FETCH_CATEGORY});
    return get('/api/categories', token).then((response) => {
      return dispatch({type: FETCH_CATEGORY_SUCCESS, response})
    }).catch((error) => {
      toastr.error(error.message);
      return dispatch({type: FETCH_CATEGORY_FAILURE});
    })
  }
}

