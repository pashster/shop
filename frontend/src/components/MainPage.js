import React                               from 'react';
import PropTypes                           from 'prop-types';
import {BootstrapTable, TableHeaderColumn} from "react-bootstrap-table";
import moment                              from "moment";
import { Link }                            from "react-router";

class MainPage extends React.Component {
  static propTypes  = {
    products: PropTypes.array.isRequired,
    isFetchingProducts: PropTypes.bool.isRequired,
    categories: PropTypes.array.isRequired,
    actions: PropTypes.shape({
      fetchProducts: PropTypes.func.isRequired,
      fetchCategory: PropTypes.func.isRequired
    })
  };

  componentWillMount() {
    this.props.actions.fetchProducts();
    this.props.actions.fetchCategory();
    if (!this.props.isLoggedIn) {
      window.location = "/login"
    }
    console.log(typeof(process.env.REACT_APP_APPLICATION_VERSION));
  };

  renderCategory = (cell, row) => {
    let category = this.showCategory(cell);
    return ( <Link to={'/category/'+cell}>{category.name}</Link>)
  };

  showCategory = (id) => {
    let currentCategory;
    this.props.categories.map((category) => {
      if (id === category.id) {
        currentCategory = category
      }
    });
    return currentCategory
  };

  renderDate = (date, row) => {
    return (
      <p>{moment(date).format('ll')}</p>
    )
  };

  render () {
    const {products} = this.props;
    const { version } = process.env.REACT_APP_APPLICATION_VERSION;
    return (
      <div>
        <h1>Привет, {localStorage.getItem('user_name')}! VERSION: {version}</h1>
        <BootstrapTable data={ products } search multiColumnSearch pagination={products.length > 11}>
          <TableHeaderColumn dataField='id' isKey>ID</TableHeaderColumn>
          <TableHeaderColumn dataField='name'>Название</TableHeaderColumn>
          <TableHeaderColumn dataField='desc'>Описание</TableHeaderColumn>
          <TableHeaderColumn dataField='cost_price'>Себестоимость</TableHeaderColumn>
          <TableHeaderColumn dataField='price'>Продажная цена</TableHeaderColumn>
          <TableHeaderColumn dataField='date_of_receipt' dataFormat={ this.renderDate }>Дата создания</TableHeaderColumn>
          <TableHeaderColumn dataField='quantity'>Колличество</TableHeaderColumn>
          <TableHeaderColumn dataField='category_id' dataFormat={ this.renderCategory }>Категория</TableHeaderColumn>
        </BootstrapTable>
        <div><strong>VERSION: </strong><i>{ process.env.REACT_APP_APPLICATION_VERSION }</i></div>
      </div>
    )
  }
}

export default MainPage;