import merge from 'xtend';
import createReducer from './create-reducer';
import {
  CHANGE_NAME, CHANGE_EMAIL, CHANGE_PASSWORD, CHANGE_PASSWORD_CONFIRMATION, REQUEST_REGISTRATION,
  REQUEST_REGISTRATION_SUCCESS, REQUEST_REGISTRATION_FAILURE
} from '../actions/auth/registration';

const INITIAL_STATE = {
  name: '',
  email: '',
  password: '',
  passwordConf: '',
  isLoading: false
};

export default createReducer({
  [CHANGE_NAME]: (state, action) => merge(state, {name: action.name}),
  [CHANGE_EMAIL]: (state, action) => merge(state, {email: action.email}),
  [CHANGE_PASSWORD]: (state, action) => merge(state, {password: action.password}),
  [CHANGE_PASSWORD_CONFIRMATION]: (state, action) => merge(state, {
    passwordConf: action.passConf
  }),
  [REQUEST_REGISTRATION]: (state, action) => merge(state, {isLoading: true}),
  [REQUEST_REGISTRATION_FAILURE]: (state, action) => merge(state, {isLoading: false}),
  [REQUEST_REGISTRATION_SUCCESS]: (state, action) => merge(state, {isLoading: false})
}, INITIAL_STATE)