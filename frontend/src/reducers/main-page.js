import merge from 'xtend';
import createReducer from './create-reducer';
import {
  FETCH_PRODUCTS, FETCH_PRODUCTS_FAILURE, FETCH_PRODUCTS_SUCCESS, FETCH_CATEGORY_FAILURE, FETCH_CATEGORY_SUCCESS, FETCH_CATEGORY
} from '../actions/main-page';


const INITIAL_STATE = {
  products: [],
  categories: [],
  isFetchingProducts: false,
  isFetchingCategories: false
};

export default createReducer({
  [FETCH_PRODUCTS]: (state, action) => merge(state, {
    isFetchingProducts: true
  }),
  [FETCH_PRODUCTS_SUCCESS]: (state, action) => merge(state, {
      products: action.response,
      isFetchingProducts: false
  }),
  [FETCH_PRODUCTS_FAILURE]: (state, action) => merge(state, {
    isFetchingProducts: false
  }),
  [FETCH_CATEGORY]: (state, action) => merge(state, {isFetchingCategories: true}),
  [FETCH_CATEGORY_SUCCESS]: (state, action) => merge(state, {
    categories: action.response,
    isFetchingCategories: false
  }),
  [FETCH_CATEGORY_FAILURE]: (state, action) => merge(state, {isFetchingCategories: false})
}, INITIAL_STATE)