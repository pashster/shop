import React                       from 'react';
import { bindActionCreators }      from "redux";
import { connect }                 from "react-redux";
import { logout }                  from '../actions/auth/login-page';
import { Menu, Responsive } from 'semantic-ui-react';
import { Container, Icon }         from 'semantic-ui-react';
import { Link }                    from "react-router";


class Layout extends React.Component {

  state = {activeItem: 'home', width: ''};

  handleItemClick = (name)=> {
    this.setState({activeItem: name});
  };

  handleLogoutClick = (e)=> {
    e.preventDefault();
    this.props.loginActions.logout();
  };

  handleOnUpdate = (e, {width})=>this.setState({width});

  render() {
    const {isLoggedIn} = this.props;
    const {activeItem, width} = this.state;
    const textAlign = width >= Responsive.onlyComputer.minWidth ? 'right' : 'left';
    return ( <Responsive
        as={Container}
        columns={1}
        fireOnMount
        onUpdate={this.handleOnUpdate}
      >
        <Container fluid>
          {isLoggedIn && <div style={{borderBottom: '1px solid'}}>

            <Menu secondary>
              <Menu.Item name='home' active={activeItem === 'home'}>
                <Link className="link_custom" to={'/'} onClick={() => this.handleItemClick("home")}>Склад</Link>
              </Menu.Item>
              <Menu.Item name='categories' active={activeItem === 'categories'}>
                <Link className="link_custom" to={'/categories'} onClick={() => this.handleItemClick("categories")}>Категории</Link>
              </Menu.Item>
              <Menu.Item name='debtors' active={activeItem === 'debtors'}>
                <Link className="link_custom" to={'/debtors'} onClick={() => this.handleItemClick("debtors")}>Должники</Link>
              </Menu.Item>
              <Menu.Menu position='right'>
                <Menu.Item onClick={this.handleLogoutClick}><Icon name="log out"/></Menu.Item>
              </Menu.Menu>
            </Menu>

          </div> }
          <br/>
          {this.props.children}
        </Container>
      </Responsive> )
  }
}

function mapStateToProps(state) {
  return state.loginPage
}

function mapDispatchToProps(dispatch) {
  return {
    loginActions: bindActionCreators({logout}, dispatch)
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(Layout)
