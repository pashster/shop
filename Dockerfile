FROM ruby:2.4.0
RUN apt-get update -qq && apt-get install -y build-essential libpq-dev nodejs
RUN gem install bundler
RUN mkdir -p /shop-app
WORKDIR /shop-app
COPY Gemfile /shop-app/Gemfile
COPY Gemfile.lock /shop-app/Gemfile.lock
RUN bundle install
COPY . /shop-app
EXPOSE 5000
CMD ["bundle", "exec", "rails", "s", "-p", "5000", "-b", "0.0.0.0"]