require 'rails_helper'

RSpec.describe Product, type: :model do
  it { should validate_presence_of(:name) }
  it { should validate_presence_of(:date_of_receipt) }
  it { should validate_presence_of(:quantity) }
  it { should validate_presence_of(:price) }
  it { should validate_presence_of(:cost_price) }
end
