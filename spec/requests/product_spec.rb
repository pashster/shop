require 'rails_helper'

RSpec.describe 'Products API', type: :request do
  let(:user) {create(:user)}
  let(:category) {create(:category)}
  let!(:products) {create_list(:product, 10, user_id: user.id, category_id: category.id)}
  let(:product_id) {products.first.id}
  let(:headers) {valid_headers}

  describe 'GET /api/products' do
    before {get '/api/products', params: {}, headers: headers}
    it 'returns products' do
      # Note `json` is a custom helper to parse JSON responses
      expect(json).not_to be_empty
      expect(json.size).to eq(10)
    end

    it 'returns status code 200' do
      expect(response).to have_http_status(200)
    end
  end

  describe 'GET /api/products/:id' do
    before {get "/api/products/#{product_id}", params: {}, headers: headers}

    context 'when the record exists' do
      it 'returns the product' do
        expect(json).not_to be_empty
        expect(json['id']).to eq(product_id)
      end

      it 'returns status code 200' do
        expect(response).to have_http_status(200)
      end
    end

    context 'when the record does not exist' do
      let(:product_id) {100}

      it 'returns status code 404' do
        expect(response).to have_http_status(404)
      end

      it 'returns a not found message' do
        expect(response.body).to match(/Couldn't find Product/)
      end
    end
  end

  describe 'POST /api/products' do
    let(:valid_attributes) do
      {name: 'Learn Elm',
       desc: 'Description',
       quantity: 50,
       price: 300,
       cost_price: 550,
       category_id: category.id,
       user_id: user.id
      }.to_json
    end

    context 'when the request is valid' do
      before {post '/api/products', params: valid_attributes, headers: headers}

      it 'creates a products' do
        expect(json['name']).to eq('Learn Elm')
      end

      it 'returns status code 201' do
        expect(response).to have_http_status(201)
      end
    end

    context 'when the request is invalid' do
      before {post '/api/products', params: {name: 'Foobar'}.to_json, headers: headers}

      it 'returns status code 422' do
        expect(response).to have_http_status(422)
      end

      it 'returns a validation failure message' do
        expect(response.body)
            .to match(/Возникли ошибки: Category не может отсутствовать/)
      end
    end
  end

  describe 'PUT /api/products/:id' do
    let(:valid_attributes) { { name: 'Shopping' }.to_json }

    context 'when the record exists' do
      before { put "/api/products/#{product_id}", params: valid_attributes, headers: headers }

      it 'updates the record' do
        expect(response.body).to be_empty
      end

      it 'returns status code 204' do
        expect(response).to have_http_status(204)
      end
    end
  end

  describe 'DELETE /api/products/:id' do
    before { delete "/api/products/#{product_id}", params: {}, headers: headers }

    it 'returns status code 204' do
      expect(response).to have_http_status(204)
    end
  end
end
