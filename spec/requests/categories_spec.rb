require 'rails_helper'

RSpec.describe 'Products API', type: :request do
  let(:user) {create(:user)}
  let!(:categories) {create_list(:category, 10)}
  let(:category_id) {categories.first.id}
  let(:headers) {valid_headers}

  describe 'GET /api/categories' do
    before {get '/api/categories', params: {}, headers: headers}
    it 'returns categories' do
      # Note `json` is a custom helper to parse JSON responses
      expect(json).not_to be_empty
      expect(json.size).to eq(10)
    end

    it 'returns status code 200' do
      expect(response).to have_http_status(200)
    end
  end

  describe 'GET /api/categories/:id' do
    before {get "/api/categories/#{category_id}", params: {}, headers: headers}

    context 'when the record exists' do
      it 'returns the category' do
        expect(json).not_to be_empty
        expect(json['id']).to eq(category_id)
      end

      it 'returns status code 200' do
        expect(response).to have_http_status(200)
      end
    end

    context 'when the record does not exist' do
      let(:category_id) {100}

      it 'returns status code 404' do
        expect(response).to have_http_status(404)
      end

      it 'returns a not found message' do
        expect(response.body).to match(/Couldn't find Category/)
      end
    end
  end

  describe 'POST /api/categories' do
    let(:valid_attributes) do
      { name: 'Learn Elm' }.to_json
    end

    context 'when the request is valid' do
      before {post '/api/categories', params: valid_attributes, headers: headers}

      it 'creates a categories' do
        expect(json['name']).to eq('Learn Elm')
      end

      it 'returns status code 201' do
        expect(response).to have_http_status(201)
      end
    end

    context 'when the request is invalid' do
      before {post '/api/categories', params: {name: 'Fo'}.to_json, headers: headers}

      it 'returns status code 422' do
        expect(response).to have_http_status(422)
      end

      it 'returns a validation failure message' do
        expect(response.body)
            .to match(/Возникли ошибки: Name недостаточной длины/)
      end
    end
  end

  describe 'PUT /api/categories/:id' do
    let(:valid_attributes) { { name: 'Shopping' }.to_json }

    context 'when the record exists' do
      before { put "/api/categories/#{category_id}", params: valid_attributes, headers: headers }

      it 'updates the record' do
        expect(response.body).to be_empty
      end

      it 'returns status code 204' do
        expect(response).to have_http_status(204)
      end
    end
  end

  describe 'DELETE /api/categories/:id' do
    before { delete "/api/categories/#{category_id}", params: {}, headers: headers }

    it 'returns status code 204' do
      expect(response).to have_http_status(204)
    end
  end
end
