FactoryBot.define do
  factory :product, class: Product do
    name { Faker::Name.name }
    desc { Faker::Lorem.sentence(5)}
    date_of_receipt { Date.current.to_formatted_s(:db) }
    quantity 64
    price 500
    category_id nil
    cost_price 800
    user_id nil
  end
end